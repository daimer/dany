//------------------------------------------------------------->
// The regular main start here !
//------------------------------------------------------------->

// RequireJS configuration.
requirejs.config({

	baseUrl: '/andre.meira/js',
    
 // aliases library/Dany/Config
    paths: { 

        "module"     : "apps/dynamix",
        "app"        : "apps/dynamix/App",
        "router"     : "apps/dynamix/Router",

        "DConfig"    : "apps/dynamix/DConfig",

        "paper"      : "library/vendor/paper-min",
        "jquery"     : "library/vendor/jquery.min",
        "backbone"   : "library/vendor/backbone.min",
        "underscore" : "library/vendor/underscore.min",
        "text"       : "library/vendor/node_modules/requirejs/text"
    },

    //modules
    packages: [
        //"module/index"
    ],

    shim: {
        "bootstrap" : {deps: ['jquery']},
        "app"       : {deps: ["backbone"]},
        "router"    : {deps: ["backbone"]},
        "backbone"  : {deps: ['underscore', 'jquery']}
    },

    deps: ["underscore", "backbone", "paper"]
});

/**
 * Application bootstrap 
 */
define(function bootstrap (require) {
    var App      = require('app');
    var Router   = require('router');   

    /**
     * On dom ready
     */
    $(function () {
        var app    = new App();
        var router = new Router();
        router.app = app.render();

        Backbone.history.start({pushState: false, root:"/html/dynamix"});
        
        if (!Backbone.history.fragment) {
            router.navigate('home', {trigger:true});
        }
    });
});
