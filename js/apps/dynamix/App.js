define(function (require) {
	
	/**
	 *
	 */
	return Backbone.View.extend({
		el: 'body',
		views : {},
		state : new Backbone.Model(), 

		/**
		 *
		 */
		render: function () {			
			this.$el.append('<section id="menu-section" />');
			this.$el.append('<section id="app-section"  />');
			return this;
		}
	});
});