define(function (require) {

	/**
	 * Reverse responsablity
	 */
	var renderView = function (view) {
		return view.render();
	};

	/**
	 * Reverse responsablity
	 */
	var renderViews = function () {
		_.each(arguments, renderView);
	};

	/**
	 *
	 */
	return Backbone.Router.extend({
		routes: {
			"home"              : "home",			
 			"road/cross"        : "crossRoad",
 			"move/brown"        : "moveBrownian",
 			"follow/simple"     : "followSimple",
 			"bees"              : "followRepulse",
 			"follow/chain"      : "followChain",
 			"follow/no-bounds"  : "followNoBounds",

 			//404 						
 			"*notFound"  : "notFound"
		},
		
		initialize: function (opt) {
			this.once('all', _.bind(this.constantModules, this));
		}, 
		
		/**
		 * On all
		 */
		constantModules: function () {
			require([
				'apps/dynamix/main-menu/main', 
				'apps/dynamix/footer/main'
			], renderViews);
		},
		
		
		/**
		 *
		 */
		home : function () {
			require(['apps/dynamix/index/main'], renderView);
		},

		/**
		 *
		 */
		crossRoad : function () {
			require(['apps/dynamix/cross-road/main'], renderView);
		},

		/**
		 *
		 */
		followChain : function () {
			require(['apps/dynamix/follow/chain/main'], renderView);
		},

		/**
		 *
		 */
		followSimple : function () {
			require(['apps/dynamix/follow/simple/main'], renderView);
		},

		/**
		 *
		 */
		followRepulse : function () {
			require(['apps/dynamix/follow/repulse/main'], renderView);
		},

		/**
		 *
		 */
		moveBrownian : function () {
			require(['apps/dynamix/follow/brownian/main'], renderView);
		},

		/**
		 *
		 */
		followNoBounds  : function () {
			require(['apps/dynamix/follow/no-bounds/main'], renderView);
		},

		/**
		 * 
		 */
		notFound : function () {
			
		}
	});
});