
/**
 * 
 */
define(function (require) {
	var content = require('text!module/follow/repulse/templates/container.html');


	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#app-section",
		memory: 0,

		events : {
            'click ' : 'toggle',
        },	

        /**
		 *
		 */
        initialize : function() {
        	this.timer = [];
            _.bindAll(this);
        },

        toggle: function () {
        	if (this.timer.length) {
        		this.stop();
        		return;
        	}
        	this.exec();
        },

        /**
		 *
		 */
        stop: function () {
    		_.each(this.timer, function (t) {
    			clearInterval(t);
    		});
    		
    		this.timer = [];
        },

        /**
		 *
		 */
		exec: function () {
			this.timer.push(setInterval(this.moveTarget,  20));
			this.timer.push(setInterval(this.moveHunters, 20));
		},


        /**
		 *
		 */
        moveTarget: function () {
        	
        	var phase = this.memory > 150 ? 1 : -1;
        	var rand  = Math.random() * 20 - 10;
        	var decay = 3 * phase;

        	this.targetSpeed = new paper.Point({
        		angle: this.targetSpeed.angle + decay + rand, length: 15
        	});

        	var position = this.target.position.add(this.targetSpeed);

			position.x = Math.min(position.x, this.max.x);
			position.y = Math.min(position.y, this.max.y);

			position.x = Math.max(position.x, this.min.x);
			position.y = Math.max(position.y, this.min.y);

			this.target.position = position;

			this.memory = (this.memory + 1) % 300;
			paper.view.draw();
        },

        /**
		 *
		 */
        moveHunters: function () {
        	  		
        	this.moveHunter(this.hunters[0], this.hunters[1]);	
        	this.moveHunter(this.hunters[1], this.hunters[2]);	
        	this.moveHunter(this.hunters[2], this.hunters[3]);	
        	this.moveHunter(this.hunters[3], this.hunters[4]);	
        	this.moveHunter(this.hunters[4], this.hunters[5]);	
        	this.moveHunter(this.hunters[5], this.hunters[6]);	
        	this.moveHunter(this.hunters[6], this.hunters[0]);	
			paper.view.draw();
        },

        /**
         *
         */
        moveHunter: function (h, r) {
        	var add = (this.memory % 100) / 20;

			var rep  = h.position.add(
				r.position.multiply(-1)
			).normalize(5);

			var attr = this.target.position.add(
				h.position.multiply(-1)
			).normalize(10);

			var vector = rep.add(attr).normalize(10 + add);
			var position = h.position.add(vector);

			position.x = Math.min(position.x, this.max.x);
			position.y = Math.min(position.y, this.max.y);

			position.x = Math.max(position.x, this.min.x);
			position.y = Math.max(position.y, this.min.y);

			h.position = position;
        },

        makeHunters: function () {
        	this.hunters = [];
			this.hunters.push(new paper.Path.Circle(this.from.add(-50), 2));
			this.hunters.push(new paper.Path.Circle(this.from.add(100), 2));
			this.hunters.push(new paper.Path.Circle(this.from.add(150), 2));
			this.hunters.push(new paper.Path.Circle(this.from.add(200), 2));
			this.hunters.push(new paper.Path.Circle(this.from.add(250), 2));
			this.hunters.push(new paper.Path.Circle(this.from.add(300), 2));
			this.hunters.push(new paper.Path.Circle(this.from.add(250), 2));

			_.each(this.hunters, _.bind(function (h) {
        		h.strokeColor = 'black';
        	}, this));
        },

        /**
         *
         */
        makeTarget: function () {
        	this.target = new paper.Path.Circle(this.from, 10);
			this.target.strokeColor = 'black';	
			this.targetSpeed = new paper.Point({angle:0, length:0});

        },

        /** 
         *
         */
        setupBounds: function () {
        	paper.setup($('#main-canvas')[0]);
        	var width  = this.$el.innerWidth()  - 40;
			var height = this.$el.innerHeight() - 40;
			
			this.times  = 0;			
			this.min    = new paper.Point(0, 0);
			this.max    = new paper.Point(width, height);
			this.from   = new paper.Point(width / 2, height / 2);
        },
		
		/**
		 *
		 */
		render: function () {
			
			this.$el.html(content);

			
			this.setupBounds();
			this.makeTarget();
			this.makeHunters();

			this.stop();
			this.exec();
			return this;
		}


	});
});
