
/**
 * 
 */
define(function (require) {
	var content = require('text!module/follow-chain/templates/container.html');


	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#app-section",
		memory: 0,

		events : {
            'click ' : 'toggle',
        },	

        /**
		 *
		 */
        initialize : function() {
        	this.timer = [];
            _.bindAll(this);
        },

        toggle: function () {
        	if (this.timer.length) {
        		this.stop();
        		return;
        	}
        	this.exec();
        },

        /**
		 *
		 */
        stop: function () {
    		_.each(this.timer, function (t) {
    			clearInterval(t);
    		});
    		
    		this.timer = [];
        },

        /**
		 *
		 */
		exec: function () {
			this.timer.push(setInterval(this.moveFollowersAttr, 20));
			this.timer.push(setInterval(this.moveFollowersRep, 20));
		},


        /**
		 *
		 */
        moveFollowersAttr: function () {
        	return;
        	  		
        	this.moveFollowerAttr(this.followers[0], this.followers[1]);	
        	this.moveFollowerAttr(this.followers[1], this.followers[2]);	
        	this.moveFollowerAttr(this.followers[2], this.followers[3]);	
        	this.moveFollowerAttr(this.followers[3], this.followers[4]);	
        	this.moveFollowerAttr(this.followers[4], this.followers[5]);	
        	this.moveFollowerAttr(this.followers[5], this.followers[6]);	
        	this.moveFollowerAttr(this.followers[6], this.followers[0]);	
			paper.view.draw();
        },

        /**
		 *
		 */
        moveFollowersRep: function () {
        	  		
        	this.moveFollowerRep(this.followers[1], this.followers[0]);	
        	this.moveFollowerRep(this.followers[2], this.followers[1]);	
        	this.moveFollowerRep(this.followers[3], this.followers[2]);	
        	this.moveFollowerRep(this.followers[4], this.followers[3]);	
        	this.moveFollowerRep(this.followers[5], this.followers[4]);	
        	this.moveFollowerRep(this.followers[6], this.followers[5]);	
        	this.moveFollowerRep(this.followers[0], this.followers[6]);	
			paper.view.draw();
        },

        /**
         *
         */
        moveFollowerAttr: function (h, r) {        	
			var rep  = r.position.add(
				h.position.multiply(-1)
			).normalize(10);

			var rand = (new paper.Point(10, 10)).multiply(Math.random()).add(-5).normalize(0);

			var vector   = rep.add(rand).normalize(2);
			var position = h.position.add(vector);

			position.x = Math.min(position.x, this.max.x);
			position.y = Math.min(position.y, this.max.y);
			position.x = Math.max(position.x, this.min.x);
			position.y = Math.max(position.y, this.min.y);

			h.position = position;
        },

		/**
         *
         */
        moveFollowerRep: function (h, r) {

			var rep  = r.position.add(
				h.position.multiply(1)
			).normalize(10);

			var rand = (new paper.Point(10, 10)).multiply(Math.random()).add(-5).normalize(0);

			var vector   = rep.add(rand).normalize(4);
			var position = h.position.add(vector);

			position.x = Math.min(position.x, this.max.x);
			position.y = Math.min(position.y, this.max.y);
			position.x = Math.max(position.x, this.min.x);
			position.y = Math.max(position.y, this.min.y);

			h.position = position;
        },
		
		/**
		 *
		 */
		render: function () {
			
			this.stop();
			this.$el.html(content);

			var width  = this.$el.width()  - 40;
			var height = this.$el.height() - 40;

			paper.setup($('#main-canvas')[0]);
			
			this.times = 0;			
			this.min   = new paper.Point(0, 0);
			this.max   = new paper.Point(width, height);
			this.from  = new paper.Point(width / 2, height / 2);

			
			this.followers = [];
			this.followers.push(new paper.Path.Circle( this.max.multiply(paper.Point.random()), 2) );
			this.followers.push(new paper.Path.Circle( this.max.multiply(paper.Point.random()), 2) );
			this.followers.push(new paper.Path.Circle( this.max.multiply(paper.Point.random()), 2) );
			this.followers.push(new paper.Path.Circle( this.max.multiply(paper.Point.random()), 2) );
			this.followers.push(new paper.Path.Circle( this.max.multiply(paper.Point.random()), 2) );
			this.followers.push(new paper.Path.Circle( this.max.multiply(paper.Point.random()), 2) );
			this.followers.push(new paper.Path.Circle( this.max.multiply(paper.Point.random()), 2) );

			_.each(this.followers, _.bind(function (h) {
        		h.strokeColor = 'black';
        	}, this));		

			this.exec();
			return this;
		}


	});
});
