
/**
 * 
 */
define(function (require) {
	var Grid    = require('library/Dynamix/Space/Grid');
	var content = require('text!module/follow/no-bounds/templates/container.html');


	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#app-section",

		events : {
            'click' : 'toggle',
        },	

        /**
		 *
		 */
        initialize : function() {
            _.bindAll(this);
        },

        /**
         * 
         */
        moveCamera: function (target) {
        	var center = paper.project.view;
        	var vector = target.position.add(
        		center.position.multiply(-1)
			);
        },

        /**
         *
         */
        toggle: function () {
        	if (this.timer.length) {
        		this.stop();
        		return;
        	}
        	this.exec();
        },

        /**
		 *
		 */
        stop: function () {
    		_.each(this.timer, function (t) {
    			clearInterval(t);
    		});
    		
    		this.timer = [];
        },

        /**
		 *
		 */
		exec: function () {			
			this.timer.push(setInterval(_.bind(function () {
				this.grid.move([5, 5]);
				paper.view.draw();
			}, this), 10));
		},

		/**
		 *
		 */
		render: function () {
			
			this.$el.html(content);
			paper.setup($('#main-canvas')[0]);
			this.grid = new Grid(paper);
			this.grid.draw();
			this.stop();
			this.exec();
			return this;
		}


	});
});
