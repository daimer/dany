/**
 * 
 */
define(function (require) {
	var content = require('text!module/follow/brownian/templates/container.html');


	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#app-section",

		events : {
            'click ' : 'stop',
        },	

        /**
		 *
		 */
        initialize : function() {
        	this.timer = [];
            _.bindAll(this);
        },

        /**
		 *
		 */
        stop: function () {
        	if (!this.timer.length) {
        		this.exec();
        		return;
        	}

    		_.each(this.timer, function (t) {
    			clearInterval(t);
    		});
    		
    		this.timer = [];
        },

        /**
		 *
		 */
        addPoint: function (point) {
        	var to  = point.multiply(paper.Point.random()); 

			to = to.add(point.multiply(-0.5));
			to = to.add(this.from);

			to.x = Math.min(to.x, this.max.x);
			to.y = Math.min(to.y, this.max.y);

			to.x = Math.max(to.x, this.min.x);
			to.y = Math.max(to.y, this.min.y);


			this.myPath.add(to);
			//this.myPath.smooth();
			this.from = to;
			paper.view.draw();
        },

        /**
		 *
		 */
		addSmallPoint: function () {
			this.addPoint(new paper.Point(10, 10));
		},

		/**
		 *
		 */
		addBigPoint: function () {
			this.addPoint(new paper.Point(100, 100));
		},

		/**
		 *
		 */
		exec: function () {
			this.timer.push(setInterval(this.addSmallPoint, 10));
			this.timer.push(setInterval(this.addBigPoint, 2000));
		},

		/**
		 *
		 */
		render: function () {
			this.stop();
			this.$el.html(content);
			var width  = this.$el.width()  - 10;
			var height = this.$el.height() - 10;

			paper.setup($('#main-canvas')[0]);
			
			this.times  = 0;
			this.myPath = new paper.Path();
			this.min    = new paper.Point(0, 0);
			this.max    = new paper.Point(width, height);
			this.from   = new paper.Point(width / 2, height / 2);

			this.myPath.strokeColor = 'black';
			this.exec();
			return this;
		}


	});
});
