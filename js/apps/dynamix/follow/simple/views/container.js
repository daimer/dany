
/**
 * 
 */
define(function (require) {
	var content = require('text!module/follow/simple/templates/container.html');


	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#app-section",
		memory: 0,

		events : {
            'click ' : 'toggle',
        },	

        /**
		 *
		 */
        initialize : function() {
        	this.timer = [];
            _.bindAll(this);
        },

        toggle: function () {
        	if (this.timer.length) {
        		this.stop();
        		return;
        	}
        	this.exec();
        },

        /**
		 *
		 */
        stop: function () {
    		_.each(this.timer, function (t) {
    			clearInterval(t);
    		});
    		
    		this.timer = [];
        },

        /**
		 *
		 */
        moveTarget: function () {
        	
        	var phase = this.memory > 150 ? 1 : -1;
        	var rand  = Math.random() * 20 - 10;
        	var decay = 3 * phase;

        	this.targetSpeed = new paper.Point({
        		angle: this.targetSpeed.angle + decay + rand, length: 15
        	});

        	var position = this.target.position.add(this.targetSpeed);

			position.x = Math.min(position.x, this.max.x);
			position.y = Math.min(position.y, this.max.y);

			position.x = Math.max(position.x, this.min.x);
			position.y = Math.max(position.y, this.min.y);

			this.target.position = position;

			this.memory = (this.memory + 1) % 300;
			paper.view.draw();
        },

        /**
		 *
		 */
        moveHunters: function () {
        	_.each(this.hunters, _.bind(function (h) {        		
        		this.moveHunter(h);
        	}, this));
			
			paper.view.draw();
        },

        /**
         *
         */
        moveHunter: function (h) {
        	var mult   = this.target.length / h.length;
        	var vector = this.target.position.add(
        		h.position.multiply(-1)
			).normalize(10 * mult);

			var position = h.position.add(vector);

			position.x = Math.min(position.x, this.max.x);
			position.y = Math.min(position.y, this.max.y);

			position.x = Math.max(position.x, this.min.x);
			position.y = Math.max(position.y, this.min.y);

			h.position = position;
        },

		/**
		 *
		 */
		exec: function () {
			this.timer.push(setInterval(this.moveTarget,  20));
			this.timer.push(setInterval(this.moveHunters, 20));
		},

		/**
		 *
		 */
		render: function () {
			this.stop();
			this.$el.html(content);
			var width  = this.$el.innerWidth()  - 40;
			var height = this.$el.innerHeight() - 40;

			paper.setup($('#main-canvas')[0]);
			
			this.times  = 0;			
			this.min    = new paper.Point(0, 0);
			this.max    = new paper.Point(width, height);
			this.from   = new paper.Point(width / 2, height / 2);

			
			this.hunters = [];
			this.hunters.push(new paper.Path.Circle(this.from.add(1200), 12));
			this.hunters.push(new paper.Path.Circle(this.from.add(100),  40));

			this.target = new paper.Path.Circle(this.from, 10);
			
			this.targetSpeed = new paper.Point({angle:0, length:0});

			this.target.strokeColor = 'black';	
			_.each(this.hunters, _.bind(function (h) {
        		h.strokeColor = 'black';
        	}, this));		

			this.exec();
			return this;
		}


	});
});
