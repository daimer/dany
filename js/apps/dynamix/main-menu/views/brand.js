/**
 * 
 */
define(function (require) {

	var html = require('text!module/main-menu/templates/brand.html');
	
	/**
	 *
	 */
	return Backbone.View.extend({
		className: "brand",

		events: {
			'click h1' : "toggleContent"
		},

		toggleContent: function () {
			this.options.state.trigger('toggle-content')
		},

		/**
		 *
		 */
		render: function () {
			this.$el.html(html);			
			return this;
		}
	});
});
