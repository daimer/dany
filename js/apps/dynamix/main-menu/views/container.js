/**
 * 
 */
define(function (require) {
	var brand   = require('module/main-menu/views/brand');
	var content = require('module/main-menu/views/content');
	var html    = require('text!module/main-menu/templates/main-menu.html');
	
	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#menu-section",

		/**
		 *
		 */
		render: function () {		

			var options = {state:this.options.state};

			this.$el.html(html);
			this.$el.find('.brand')  .html((new brand(options))  .render().$el);
			this.$el.find('.content').html((new content(options)).render().$el);
			return this;
		}
	});
});
