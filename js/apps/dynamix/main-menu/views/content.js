/**
 * 
 */
define(function (require) {

	var html = require('text!module/main-menu/templates/content.html');
	
	/**
	 *
	 */
	return Backbone.View.extend({
		/**
         *
         */
        initialize: function() {
        	_.bindAll(this);
        	this.options.state.on('toggle-content', this.onToggle);  
		},

		onToggle: function () {
			this.$el.closest('.content').toggleClass('enabled');
		},

		/**
		 *
		 */
		render: function () {
			this.$el.html(html);			
			return this;
		}
	});
});
