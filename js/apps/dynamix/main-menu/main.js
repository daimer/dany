/**
 * 
 */
define(function (require) {
	var state  = new Backbone.Model();
	return new (require('apps/dynamix/main-menu/views/container'))({state:state});
});
