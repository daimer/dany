/**
 * 
 */
define(function (require) {
	var state  = new Backbone.Model();
	return new (require('apps/dynamix/cross-road/views/container'))({state:state});
});
