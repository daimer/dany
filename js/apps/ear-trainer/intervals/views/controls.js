/**
 * 
 */
define(function (require) {
	var content          = require('text!module/intervals/templates/controls.html');
	var motionControls   = require('text!module/intervals/templates/controls/motion.html');
	var rootControls     = require('text!module/intervals/templates/controls/root-note.html');
	var playControls     = require('text!module/intervals/templates/controls/play-control.html');
	var octaveControls   = require('text!module/intervals/templates/controls/octave-from.html');
	var rangeControls    = require('text!module/intervals/templates/controls/octave-range.html');
	var disableControls  = require('text!module/intervals/templates/controls/disable-intervals.html');

	/**
	 *
	 */
	return Backbone.View.extend({
		tag: "div",
		id: "controls-box",

		/**
		 * 
		 */
		events: {
			'click #intervals-play'                       : "play",
			'click #intervals-stop'                       : "stop",
			'click #intervals-start'                      : "start",
			'click .intervals-motion ul li a'             : "motion",
			'click .intervals-root-note ul li a'          : "rootNote",			
			'click .intervals-octave-from ul li a'        : "octaveFrom",
			'click .intervals-octave-range ul li a'       : "octaveRange",
			'click .enabled #intervals-disable-intervals' : "disableIntervals",
            
        },

        /**
         *
         */
        initialize: function() {
        	this.options.state.on('change:mode',  _.bind(this.onChangeMode,  this));  
			this.options.state.on('change:tries', _.bind(this.onChangeScore, this));  
			this.options.state.on('answer:wrong', _.bind(this.onWrongAnswer, this));  
			this.options.state.on('answer:right', _.bind(this.onCorrectAnswer, this));

		},

        /**
		 *
		 */
		start: function (e, data) { 
			this.options.state.trigger('clicked:start');
		},

		/**
		 *
		 */
		stop: function (e, data) { 
			this.options.state.trigger('clicked:stop');
			$('#intervals-play').text('play');
		},

		/**
		 *
		 */
		play: function (e, data) { 
			$(e.target).closest('button').text('replay');
			this.options.state.trigger('clicked:play');
		},

		/**
		 *
		 */
		onCorrectAnswer: function () {
			this.$el.find('#intervals-errors').empty();
			this.$el.find('#intervals-play').text('next');
			this.$el.find('#intervals-score').css({color:'#7EC97A'});

			_.each(this.options.state.get('errors'), _.bind(function (e, name) {				
				this.$el.find('#intervals-errors')
					.append('<li>' + name+' not found ' + e['global'] + ' time(s)</li>');
			}, this));
		},

		/**
		 *
		 */
		onWrongAnswer: function () {						
			this.$el.find('#intervals-score').css({color:'#D88'});
		},

		/**
		 *
		 */
		rootNote: function (e, data) { 
			var text = $(e.target).closest('a').text();
			var note = $(e.target).closest('a').attr('data-note');

			this.$el.find('.current-root-note').text(text);
			this.options.state.set('root-note', note);
		},

		/**
		 *
		 */
		motion: function (e, data) {
			var text   = $(e.target).closest('a').text();
			var motion = $(e.target).closest('a').attr('data-motion');

			this.$el.find('#intervals-current-motion').text(text);
			this.options.state.set('motion', motion);
		},

		/**
		 *
		 */
		octaveFrom: function (e, data) { 			
			$(e.target).closest('ul').find('li').removeClass('active');
			$(e.target).closest('li').addClass('active');

			var octave = parseInt($(e.target).closest('a').attr('data-octave'), 10);
			this.options.state.set('octave-from', octave);
		},

		/**
		 *
		 */
		octaveRange: function (e, data) { 			
			$(e.target).closest('ul').find('li').removeClass('active');
			$(e.target).closest('li').addClass('active');

			var octave = parseInt($(e.target).closest('a').attr('data-octave'), 10);
			this.options.state.set('octave-range', octave);
		},

		/**
         *
         */
		onChangeMode: function () {
			this.options.state.get('mode') == 'game' ?
				this.setGameMode() :
				this.setSettingsMode();
		},

		/**
         *
         */
		onChangeScore: function () {
			var score = this.options.state.get('score') || 0;
			var tries = this.options.state.get('tries') || 0;			

			if (tries === 0) {
				this.$el.find('#intervals-errors').empty();
				this.$el.find('#intervals-score').empty();
				return;
			}

			this.$el.find('#intervals-score').html(
				'score: ' + score + ' / ' + tries
			);
		},

		/**
		 *
		 */
		setGameMode: function () {
			this.$el.find('.intervals-play-toolbox').show();
			this.$el.find('.intervals-start-toolbox').hide();
			this.$el.find('#intervals-settings').removeClass('enabled');
			this.$el.find('#intervals-settings a').addClass('disabled');
			this.$el.find('#intervals-settings button').addClass('disabled');
		},

		/**
		 *
		 */
		setSettingsMode: function () {
			this.$el.find('.intervals-play-toolbox').hide();
			this.$el.find('.intervals-start-toolbox').show();
			this.$el.find('#intervals-settings').addClass('enabled');
			this.$el.find('#intervals-settings a').removeClass('disabled');
			this.$el.find('#intervals-settings button').removeClass('disabled');
		},

		/**
		 *
		 */
		disableIntervals: function (e, data) { 
			if (this.options.state.get('mode') == 'disable-intervals') {
				this.options.state.set('mode', null);	
				return;
			}
			this.options.state.set('mode', 'disable-intervals');
		},

		/**
		 *
		 */
		render: function () {
			this.$el.html(content);

			this.$el.find('#intervals-settings')
				.append(disableControls)
				.append(rootControls)
				.append(octaveControls)
				.append(rangeControls)
				.append(motionControls);

			this.$el.find('#intervals-player-controls')
				.append(playControls);

			return this;
		}
	});
});
