/**
 * 
 */
define(function (require) {
	var controlsView  = require('module/intervals/views/controls');
	var intervalsView = require('module/intervals/views/intervals');
	var content = require('text!module/intervals/templates/container.html');



	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#app-section",

		/**
		 *
		 */
		render: function () {
			var options = {state:this.options.state};

			this.$el.html(content);

			this.$el.find('.span7').empty()
				.append((new controlsView(options)).render().$el);
			this.$el.find('.span5').empty()
				.append((new intervalsView(options)).render().$el);
				
				
				

			return this;
		}
	});
});
