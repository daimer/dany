/**
 * 
 */
define(function (require) {
	var state  = new Backbone.Model();
	var engine = new (require('module/intervals/engine'))(state);
	return new (require('module/intervals/views/container'))({state:state});
});
