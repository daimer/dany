//------------------------------------------------------------->
// The regular main start here !
//------------------------------------------------------------->

// RequireJS configuration.
requirejs.config({

	baseUrl: '/andre.meira/js',

    // aliases
    paths: {
	"module"     : "apps/ear-trainer",
        "app"        : "apps/ear-trainer/App",
        "router"     : "apps/ear-trainer/Router",
        "DConfig"    : "apps/ear-trainer/NCNDConfig",
        "bootstrap"  : "library/vendor/bootstrap.min",
	"jquery"     : "library/vendor/jquery.min",
	"backbone"   : "library/vendor/backbone.min",
	"underscore" : "library/vendor/underscore.min",
        "text"       : "library/vendor/node_modules/requirejs/text"
	},

    //modules
    packages: [
        //"module/index"
    ],

    shim: {
        "bootstrap" : {deps: ['jquery']},
        "app"       : {deps: ["backbone"]},
        "router"    : {deps: ["backbone"]},
        "backbone"  : {deps: ['underscore', 'jquery']}
    },

    deps: ["underscore", "backbone", "bootstrap"]
});

/**
 * Application bootstrap
 */
define(function bootstrap (require) {
    var App      = require('app');
    var Router   = require('router');

    /**
     * On dom ready
     */
    $(function () {
        var app    = new App();
        var router = new Router();
        router.app = app.render();

        Backbone.history.start({pushState: false, root:"/andre.meira/html/ear-trainer"});

        if (!Backbone.history.fragment) {
		router.navigate('home', {trigger:true});
        }
    });
});
