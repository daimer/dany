/**
 * 
 */
define(function (require) {
	var brand = require('text!module/header/templates/brand.html');
	
	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#app-header",

		/**
		 *
		 */
		render: function () {
			this.$el.html(brand);
			
			this.render = function () {
				return this;
			};

			return this;
		}
	});
});
