/**
 * 
 */
define(function (require) {
	var nav = require('text!module/header/templates/navigation.html');
	/**
	 *
	 */
	return Backbone.View.extend({
		tag : "div",
		className: "navbar navbar-inverse navbar-fixed-top",

		/**
		 *
		 */
		render: function () {
			this.$el.html(nav);
			return this;
		}
	});
});
