define(function (require) {

	/**
	 * Reverse responsablity
	 */
	var renderView = function (view) {
		return view.render();
	};

	/**
	 * Reverse responsablity
	 */
	var renderViews = function () {
		_.each(arguments, renderView);
	};

	/**
	 *
	 */
	return Backbone.Router.extend({
		routes: {
			"home"       : "home",
			"melody"     : "melody",
			"intervals"	 : "intervals",
			"*notFound"  : "notFound",
		},
		
		initialize: function (opt) {
			this.on('all', _.bind(this.all, this));
		}, 
		
		/**
		 * On all
		 */
		all: function () {
			require([
				'apps/ear-trainer/header/main', 
				'apps/ear-trainer/footer/main'
			], renderViews);
		},
		
		
		/**
		 *
		 */
		home : function () {
			require([
				'apps/ear-trainer/index/main'
			], renderView);
		},

		/**
		 *
		 */
		intervals : function () {
			require([
				'apps/ear-trainer/intervals/main'
			], renderView);
		},

		/**
		 *
		 */
		melody : function () {
			require([
				'apps/ear-trainer/melody/main'
			], renderView);
		},
		
		/**
		 * 
		 */
		notFound : function () {
			
		}
	});
});