/**
 * 
 */
define(function (require) {
	var controlsView  = require('module/melody/views/controls');
	var intervalsView = require('module/melody/views/intervals');
	var content = require('text!module/melody/templates/container.html');



	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#app-section",

		/**
		 *
		 */
		render: function () {
			
			var options = {state:this.options.state};		

			this.$el.html(content);
			this.$el.find('.span6:first').empty()
				.append((new controlsView(options)).render().$el);

			return this;
				
			
			this.$el.find('.span6 + .span6').empty()
				.append((new intervalsView(options)).render().$el);				

			
		}
	});
});
