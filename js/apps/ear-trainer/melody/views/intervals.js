/**
 * 
 */
define(function (require) {
	var content = require('text!module/intervals/templates/intervals.html');

	/**
	 * Transform the event into an interval name
	 */
	var intervalNameFromElem = function (elem) {
		return $(elem).closest('.intervals').attr('id')
			.replace('intervals-', '')
			.replace('-', ' ');
	};

	/**
	 *
	 */
	return Backbone.View.extend({
		tag: "div",
		id: "intervals-box",

		/**
		 * 
		 */
		events: {
            'click .intervals' : "intervalClicked"
            
        },

        /**
         *
         */
        initialize: function() {
			this.options.state.on('change:mode', _.bind(this.changeMode, this));    
		},

		/**
         *
         */
		changeMode: function () {
			if (this.options.state.get('mode') == 'disable-intervals') {
				this.$el.addClass('disable-mode');
				return;
			}

			this.$el.removeClass('disable-mode');
		},

        /**
         *
         */
		intervalClicked: function (e, data) { 
			if (this.options.state.get('mode') == 'disable-intervals') {
				return this.disableInterval(e);
			}

			this.options.state.trigger('clicked:interval', {
					interval: intervalNameFromElem(e.target)
			});
		},

		/**
		 *
		 */
		disableInterval: function (e) {
			$(e.target).closest('div.intervals').toggleClass('disabled');
			var allowed = this.$el.find('.intervals:not(.disabled)').map(function () {
				return intervalNameFromElem($(this));
			});
			this.options.state.set('allowed-intervals', allowed);
		},

		/**
		 *
		 */
		render: function () {
			this.$el.html(content);
			return this;
		}
	});
});
