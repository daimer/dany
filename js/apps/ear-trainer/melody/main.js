/**
 * 
 */
define(function (require) {
	var state  = new Backbone.Model();
	var engine = new (require('module/melody/engine'))(state);
	return new (require('module/melody/views/container'))({state:state});
});
