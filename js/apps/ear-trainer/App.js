define(function (require) {
	
	/**
	 *
	 */
	return Backbone.View.extend({
		el: 'body',
		views : {},
		state : new Backbone.Model(), 

		/**
		 *
		 */
		render: function () {
			this.$el.append('<header  id="app-header"  />');
			this.$el.append('<section id="app-section" />');
			this.$el.append('<footer  id="app-footer"  />');
			return this;
		}
	});
});