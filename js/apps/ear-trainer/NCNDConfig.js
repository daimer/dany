define(function (require) {

	return {
		baseUrl  : '/andre.meira',

		mediaUrl : function () {
			return this.baseUrl + '/media';
		},

		soundsUrl: function () {
			return this.mediaUrl() + '/sounds';
		},

		soundsExtSupported: function () {

		}
	};

});