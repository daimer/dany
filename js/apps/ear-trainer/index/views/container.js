/**
 * 
 */
define(function (require) {
	var content = require('text!module/index/templates/container.html');


	/**
	 *
	 */
	return Backbone.View.extend({
		el: "#app-section",

		/**
		 *
		 */
		render: function () {
			this.$el.html(content);
			return this;
		}
	});
});
