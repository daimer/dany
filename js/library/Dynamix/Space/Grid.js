/**
 * 
 */
define(function (require) {
	var noop      = function () {};
	var Class     = require('library/Dynamix/Class');
	
	
	/**
	 * Class sequencer 	
	 */
	return new Class({		
		crosses: [],
		paper: null,
		divisor: 2,

		/**
		 * Constructor
		 */
		initialize: function (paper) {
			this.paper = paper;
			this.width  = paper.project.view.bounds.width;
            this.height = paper.project.view.bounds.height;	
		},

		/**
		 *
		 */
		draw: function () {
			this.remove();
			this.drawCrosses();			
			this.colorizePathes();
			window.dynamixCrosses = this.crosses;
		},

		/**
		 *
		 */
		drawCrosses: function () {

			var modulo = this.divisor + 1;
			var square = Math.pow(modulo, 2);

			var width  = this.width  / this.divisor;
			var height = this.height / this.divisor;
			

			for (var i = 0; i < square; i++) {
				this.drawCross(
					width  * (i % modulo), 
					height * Math.floor(i / modulo)
				);
			}
		},

		/**
		 *
		 */
		colorizePathes: function (color) {
			_.each(this.crosses, function (c) {
				c.y.strokeWidth = 2;
				c.x.strokeWidth = 2;
				c.x.strokeColor = color || '#c88';
				c.y.strokeColor = color || '#c88';
			});
		},

		/**
		 *
		 */
		drawCross: function (x, y)
		{
			this.crosses.push({
				"x": this.drawCrossPart([x, y - 20], [x, y + 20]),
				"y": this.drawCrossPart([x - 20, y], [x + 20, y])
			});
			
		},

		/**
		 *
		 */
		drawCrossPart: function (start, end, type) {
			return new paper.Path.Line(start, end);
		},

		/**
		 *
		 */
		remove: function () {
			_.each(this.crosses, function (c) {
				c.x.remove();
				c.y.remove();
			});

			this.crosses = [];
		},

		/**
		 *
		 */
		move: function (vector) {
			for (var i = 0, l = this.crosses.length; i < l; i++) {
				this.moveCross(this.crosses[i], vector);
			}
		},

		/**
		 *
		 */
		moveCross: function (cross, vector) {
			cross.x.translate(vector);
			cross.y.translate(vector);
			this.keepCrossInBounds(cross);
		},

		/**
		 *
		 */
		keepCrossInBounds: function (cross) {

			var decay = 0;
			var vTolerance = (this.height / this.divisor) / 2;
			var hTolerance = (this.width  / this.divisor) / 2;

			if (cross.x.position.x > this.width + hTolerance) {
				decay = -cross.x.position.x - hTolerance;
				cross.x.translate([decay, 0]);
				cross.y.translate([decay, 0]);
			}

			if (cross.x.position.y > this.height + vTolerance) {
				decay = -cross.x.position.y - vTolerance;
				cross.x.translate([0, decay]);
				cross.y.translate([0, decay]);
			}

			if (cross.x.position.x < 0 - hTolerance) {
				decay = cross.x.position.x + hTolerance;
				cross.x.translate([decay, 0]);
				cross.y.translate([decay, 0]);
			}

			if (cross.x.position.y < 0 - vTolerance) {
				decay = cross.x.position.y + vTolerance;
				cross.x.translate([0, decay]);
				cross.y.translate([0, decay]);
			}
		}

	});
});