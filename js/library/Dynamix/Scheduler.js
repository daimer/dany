/**
 * 
 */
define(function (require) {
	var noop      = function () {};
	var Class     = require('library/Dany/Class');
	
	
	/**
	 * Class sequencer 	
	 */
	return new Class({

		/**
		 * Constructor
		 */
		initialize: function (frequency) {
			this.callbacks = [];
			this.timer     = null;	
			this.setFrequency(frequency);		
			
		},

		/**
		 *
		 */
		setFrequency: function (frequency) {
			this.frequency = frequency;
			this.interval  = 1000 / frequency;
		},

		/**
		 *
		 */
		onTick: function (c) {
			this.callbacks.push(c);
		},

		/**
		 *
		 */
		execute: function (token) {
			_.each(this.callbacks, function (c) {
				c(token);
			});
		},

		/**
		 *
		 */
		run : function (token) {
			var that = this, runner = function (t) {
				that.execute(t);
				that.timer = setTimeout(runner, that.interval);
			};

			runner(token || {});
		},

		/**
		 *
		 */
		stop: function () {
			clearTimeout(this.timer);
		}
	});
});