/**
 * Class
 * Utility to make objects behave like in class-based language
 */

define(function (require) {
	
	
	return {
		baseUrl  : '',
		
		mediaUrl : function () {
			return this.baseUrl + '/media';
		},
		
		soundsUrl: function () {
			return this.mediaUrl() + '/sounds';
		},
		
		soundsExtSupported: function () {
			
		}
	};
	
});